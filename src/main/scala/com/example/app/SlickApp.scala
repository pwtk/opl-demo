package com.example.app
import java.util.{Base64, UUID}


import scala.language.postfixOps
import scala.concurrent.{Await, Future}
import org.json4s.{DefaultFormats, Formats, JObject}
import org.scalatra.json.JacksonJsonSupport
import org.scalatra._
import org.scalatra.CorsSupport
import slick.jdbc.PostgresProfile.api._
import com.roundeights.hasher.Implicits._
import org.scalatra.servlet.{FileUploadSupport, MultipartConfig, SizeConstraintExceededException}
import java.io.File
import java.io.FileOutputStream


import org.json4s._

import scala.concurrent.duration.Duration



object Tables {

  class Users(tag: Tag) extends Table[( String, String)](tag, "USERS") {

    //    def id = column[Int]("U_ID", O.PrimaryKey) // This is the primary key column
    def username = column[String]("U_NAME",O.PrimaryKey)
    def password = column[String]("PASSWORD")

    // Every table needs a * projection with the same type as the table's type parameter
    def * = (username, password)
  }


  class Products(tag: Tag) extends Table[(String, String, String, String, String, String)](tag, "PRODUCTS") {
    def pid = column[String]("P_ID", O.PrimaryKey)
    def price = column[String]("PRICE")
    def username = column[String]("USERNAME")
    def name = column[String]("NAME")
    def description = column[String]("DESCRIPTION")
    def imageUrl = column[String]("IMAGEURL")
    def * = (pid, price, username, name, description, imageUrl)

    def user = foreignKey("UP_FK", username, users)(_.username)
  }

  class Tokens(tag: Tag) extends Table[(String, String)](tag, "TOKENS"){
    def tid = column[String]("T_ID", O.PrimaryKey)
    def username =column[String]("USERNAME")
    def * = (tid, username)

    def user = foreignKey("UP_FK", username, users)(_.username)
  }

  val products = TableQuery[Products]
  val users = TableQuery[Users]
  val tokens = TableQuery[Tokens]

  def getAllProducts() = products.map(u => (u.pid,u.price,u.username,u.name,u.description,u.imageUrl) )

  def getProductOwner(name: Rep[String]) = products.filter(_.username === name)

  def checkpassword(username: Rep[String]) = users.filter(_.username === username).map((u => (u.password)))

  def isUsernameExists(username: Rep[String]) = users.filter(_.username === username).exists

  def getUsername(username: Rep[String]) = users.filter(_.username === username)

  def removeProductbyId(product_uuid: Rep[String]) = products.filter(_.pid === product_uuid)

  def isProductExists(pid: Rep[String]) = products.filter(_.pid === pid).exists

  def isTokenExists(token: Rep[String]) = tokens.filter(_.tid === token).map(( u => (u.tid)))

  def getImageFile(pid: Rep[String]) = products.filter(_.pid === pid).map((u => (u.imageUrl)))

  val findproductsWithUsers = {
    for {
      p <- products
      u <- p.user
    } yield (p, u.username)
  }

  val createSchemaAction = (users.schema ++ products.schema ++ tokens.schema ).create
  val dropSchemaAction = (users.schema  ++ products.schema ++ tokens.schema ).drop
  val createDatabase = DBIO.seq(createSchemaAction)
}



trait SlickRoutes extends ScalatraBase with FutureSupport {

  def db: Database

  get("/db/create-db") {
    db.run(Tables.createDatabase)
  }

  get("/db/create-sc") {
    db.run(Tables.createSchemaAction)
  }

  get("/db/drop-db") {
    db.run(Tables.dropSchemaAction)
  }
//  get("/db/insert"){
//    db.run(Tables.insertSupplierAndCoffeeData)
// }


}

case class User(username: String, password: String)
case class UserRegis(username: String, password: String)
//case class UserRegis(username: String, password: String, address: String, email: String, name: String, phone: String)
case class ProductAdd(price: String, username: String, name: String, description: String, imageurl: String)

case class productjs(pid: String , price: String,username: String,name: String,description: String,Imageurl: String)
//id,user,pass,address,name,email,phone


class SlickApp(val db: Database) extends ScalatraServlet with FutureSupport with SlickRoutes with JacksonJsonSupport with CorsSupport with FileUploadSupport {


  configureMultipartHandling(MultipartConfig(maxFileSize = Some(3 * 1024 * 1024)))

  protected implicit def executor = scala.concurrent.ExecutionContext.Implicits.global

  protected implicit lazy val jsonFormats: Formats = DefaultFormats



  before() {
    contentType = formats("json")
  }

  options("/*") {
    response.setHeader(
      "Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers")
    )
    response.setHeader("Access-Control-Allow-Origin", request.getHeader("Access-Control-Allow-Origins"))
  }


// tbd authenticate function
//  def isauthenticated(token: String): Boolean = {
//    val checktoken = Compiled(Tables.isTokenExists _)
//    val action = db.run(checktoken(token).result)
//    val result = Await.result(action, Duration.Inf)
//
//    if (result(0) == token) {
//      true
//
//    }
//    else {
//      false
//      halt(401, "You are not authorized")
//    }
//  }

  def saltHash(password: String): String = password.salt("iloveopl").sha256.hex

  //  println( "Salted MD5: " + hashMe.salt("sweet").md5.hex )

  post("/register") {
    val u = parsedBody.extract[UserRegis]
    val checkuser = Compiled(Tables.isUsernameExists _)
    val rs = db.run(checkuser(u.username).result)
    val b: Boolean = Await.result(rs, Duration(5, "seconds"))
    if (b) {
      renderResponse(401, "User already exists")
    }
    else {
      val createUser = DBIO.seq(Tables.users += (u.username, saltHash(u.password)))
      //        u.address, u.email, u.name, u.phone))
      db.run(createUser)
    }
  }
  post("/login") {
    val u1 = parsedBody.extract[User] //    u1.username //u1.password
    val checkuser = Compiled(Tables.isUsernameExists _)
    val rs = db.run(checkuser(u1.username).result)
    val b: Boolean = Await.result(rs, Duration(5, "seconds"))
    if (b) {

      val getpassword = Compiled(Tables.checkpassword _)
      val pwd = db.run(getpassword(u1.username).result)
      val r = Await.result(pwd, Duration.Inf)
      val tostring = r.mkString(" ")
      if (tostring == saltHash(u1.password)) {
        val uuid = UUID.randomUUID().toString
        db.run(DBIO.seq(Tables.tokens += (uuid, u1.username)))
        response.addHeader("token", uuid)
        renderResponse(200,"Login Success")
      }
      else {
        renderResponse(404, "Incorrect Password")
      }
    }
  }

  get("/all") {

      val action = db.run(Tables.getAllProducts.result)
      val rs = Await.result(action, Duration(5, "seconds"))
      rs

  }


    post("/user/products/:username") {
      val username = params.getAs[String]("username").getOrElse(
        halt(BadRequest("Please provide a correct id"))
      )

      val compiledGet = Compiled(Tables.getProductOwner _)
      val action = db.run(compiledGet(username).result)
      val rs: Seq[(String,String,String,String,String,String)] = Await.result(action, Duration(5, "seconds"))
      rs
    }

    post("/products/add") {
      val content = request.body
      val json = parse(content)
      val title = (json \ "title").extract[String]
      val price = (json \ "price").extract[String]
      val description = (json \ "description").extract[String]
      val image = (json \ "image").extract[String]
      val username = (json \ "username").extract[String]
      val pid = UUID.randomUUID().toString
      val decode = java.net.URLDecoder.decode(image, "UTF-8")
      val decoded = decode.substring(28)
      val realDecoded = Base64.getDecoder.decode(decoded)
      renderResponse(200)


      val imageurl = "/Users/pwtk/Desktop/opltemp/" +pid + ".jpg"
      val file = new File("/Users/pwtk/Desktop/opltemp/", pid + ".jpg")
      val fos = new FileOutputStream(file)
      fos.write(realDecoded)
      fos.close()

      val addProduct = DBIO.seq(Tables.products += (pid, price, username, title, description, imageurl)) //save to local path
      db.run(addProduct)
    }

    //get image
    get("/products/:pid"){
      val pid = params.getAs[String]("pid").getOrElse(
        halt(BadRequest("Please provide a correct pid"))
      )
      val a = db.run(Tables.getImageFile(pid).result)
      val rs = Await.result(a, Duration.Inf)
      contentType = "application/octet-stream"
      val file = new java.io.File(rs(0).toString)
      response.setHeader("Content-Disposition", "attachment; filename=" + file.getName)
      file
    }

      get("/products/remove/:productid") {
        val productid = params.getAs[String]("productid").getOrElse(
          halt(BadRequest("Product doesn't exists"))
        )
        val isProductExists = Compiled(Tables.isProductExists _)
        val result = db.run(isProductExists(productid).result)
        val r: Boolean = Await.result(result, Duration(5, "seconds"))
        if (r) {
          val delete = Compiled(Tables.removeProductbyId _)
          db.run(delete(productid).delete)
          renderResponse(200)
        }
        else {
          "product doesn't exists"
        }
      }
}
