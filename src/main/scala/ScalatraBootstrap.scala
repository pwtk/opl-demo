
import org.scalatra._
import javax.servlet.ServletContext

import com.mchange.v2.c3p0.ComboPooledDataSource
import org.slf4j.LoggerFactory
import com.example.app._
import slick.jdbc.PostgresProfile.api._



class ScalatraBootstrap extends LifeCycle {



  val logger = LoggerFactory.getLogger(getClass)

  val cpds = new ComboPooledDataSource

  logger.info("Created c3p0 connection pool")
  val db = Database.forDataSource(cpds, None)


  override def init(context: ServletContext) {
    context.mount(new SlickApp(db), "/*")
    context.initParameters("org.scalatra.cors.allowedOrigins") = "http://localhost:3000"
  }
  def closeDbConnection() {
      logger.info("Closing c3po connection pool")
      cpds.close
  }

  override def destroy(context: ServletContext) {
    super.destroy(context)
    closeDbConnection
  }





}
