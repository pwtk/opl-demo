# Shopping-app #

## Build & Run ##

```sh
$ cd Shopping_app
$ ./sbt
> jetty:start
> browse
```
Need to config the Database before running the backend.
If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.


All of the main code is in SLICKAPP.scala
Database initialization and controller mounting is in ScalatraBootstrap.scala
Database pool configuration is in /resources/c3p0.properties
Need to have PostgresDB running in order to get a working Database.

