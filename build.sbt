val ScalatraVersion = "2.6.1"

organization := "com.example"

name := "Shopping-app"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.12.4"

resolvers += Classpaths.typesafeReleases

libraryDependencies ++= Seq(
  "org.scalatra"            %% "scalatra"          % ScalatraVersion,
  "org.scalatra"            %% "scalatra-scalate"  % ScalatraVersion,
  "org.scalatra"            %% "scalatra-specs2"   % ScalatraVersion    % "test",
  "com.typesafe.slick"      %% "slick"             % "3.2.1",
  "com.h2database"          %  "h2"                % "1.4.196",
  "com.mchange"             %  "c3p0"              % "0.9.5.2",
  "org.eclipse.jetty"       %  "jetty-webapp"      % "9.4.6.v20170531"  % "provided",
  "javax.servlet"           %  "javax.servlet-api" % "3.1.0"            % "provided",
  "org.scalatra"            %% "scalatra-json"     % ScalatraVersion,
  "org.json4s"              %% "json4s-jackson"    % "3.5.2",
  "org.slf4j"               % "slf4j-api"          % "1.7.25",
  "postgresql"              % "postgresql"         % "9.1-901-1.jdbc4",
  "org.eclipse.jetty"       % "jetty-webapp"       % "9.2.15.v20160210" % "container",
  "org.eclipse.jetty"       % "jetty-webapp"       % "9.2.15.v20160210" % "container;compile",
  "com.roundeights"         %% "hasher"            % "1.2.0",
  "org.scalatra"            %% "scalatra-auth"     % "2.6.2",
  "com.github.seratch"      %% "awscala"           % "0.6.+",
  "org.json4s"              %% "json4s-native"     % "3.6.0-M1",
  "io.spray" %%  "spray-json" % "1.3.3"
)




enablePlugins(SbtTwirl)
enablePlugins(ScalatraPlugin)
